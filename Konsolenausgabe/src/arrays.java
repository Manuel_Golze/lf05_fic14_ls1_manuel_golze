class Array {
	
 public static void main(String[] args) {
	 
	
 double [] temperatur = { 6.d0, 3.2, 2.5, 1.9, 1.6, 1.1, 0.3, 6.0,
 1.0, 4.9, 6.8, 13.5, 16.1, 15.3, 16.6, 17.1, 
 17.6, 18.2, 17.2, 15.8, 13.6, 13.2, 13.3, 12.0 };
 
 // Ausgabe in der Konsole 
 System.out.println(berechneMittelwert(temperatur));
 }
 
 
 
 // Deklaration der Funktion berechneMittelwert:
 static double berechneMittelwert(double[] array) {
double mittelwert = 0.0;
 
 for (int i = 0; i < array.length; i++) {
 mittelwert += array[i];
 }
 
 mittelwert /= array.length;
 return mittelwert;
 
 
 }
}
