public class Konsolenausgabe {

public static void main(String[] args) {
	
	//Das ist ein Kommentar 
	System.out.println("Das ist ein Test. "
			+ "Das ist der zweite Test.");
	
	//"print()" ist die Normale Ausgabe, "println()" ist die Ausgabe in einer neuen Zeile
	System.out.println("Das ist der dritte Test.");
	System.out.println("Das ist der vierte Test.");
	
	//Aufgabe 2
	System.out.println("  *");
	System.out.println("  **");
	System.out.println(" ****");
	System.out.println(" ****");
	System.out.println("******");
	System.out.println("*******");
	System.out.println("  **");
	System.out.println("  **");
	System.out.println("  **");
	
	//Aufgabe 3
	double a = 22.4234234;
	double b = 111.2222;
	double c = 4.0;
	double d = 1000000.551;
	double e = 97.34;
	System.out.printf("|%.2f|\n" , 		a);
	System.out.printf("|%.2f|\n" , 		b);
	System.out.printf("|%.2f|\n" , 		c);
	System.out.printf("|%.2f|\n" , 		d);
	System.out.printf("|%.2f|\n" , 		e);
	
	
	
	//Uebung.2
	//A.1
	System.out.println("     **");
	System.out.println("  *	 *");
	System.out.println("  *	 *");
	System.out.println("     **");
	
	//A.2
	
	System.out.println("0!     =         =   1");
	System.out.println("1!     =1        =   1");
	System.out.println("2!     =1*2      =   2");
	System.out.println("3!     =1*2*3    =   6");
	System.out.println("4!     =1*2*3*4  =  24");
	System.out.println("5!     =1*2*3*4*5= 120");
	
	//A.3
	
	System.out.println("Fahrenheit     Celsius");
	
	
	System.out.printf( "|%-20s|\n", -20); System.out.printf( "|%20s|\n", -28.8889 );
	System.out.printf( "|%-20s|\n", -10); System.out.printf( "|%20s|\n", -23.3333 );
	System.out.printf( "|%-20s|\n",   0); System.out.printf( "|%20s|\n", -17.7778 );
	System.out.printf( "|%-20s|\n",  20); System.out.printf( "|%20s|\n",  -6.6667 );
	System.out.printf( "|%-20s|\n",  30); System.out.printf( "|%20s|\n",  -1.1111 );
	

	
	
	
	
	
	
	}
}