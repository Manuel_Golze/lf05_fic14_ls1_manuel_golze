//Endlosschleife/Schleife/Z�hlung
//Autor@M.Golze/11.01.2022

class WHSchleifenWhile {
	public static void main(String[] args) {
		
		for (int i = 10; i > 0; i ++) {
			//"'for'es wird eine for Schleife erstellt"
			//"es wird i inizialisiert mit dem wert 10"
			//"'i>0' solange i gr��er als 0 ist, erh�he i um 1"
			//"da 10 immer gr��er als 0 ist wird der Prozess endloss ausgef�hrt"
			System.out.println(i);
			//System Text ausgabe des Wertes//

			}
		
		////////////////////////////////////////////////////////////////////////////////
		
		
		for  (int i = 11; i > 1; i--) {
			//"selbe spiel wie oben blo� das der wert verringert wird um 1"
			//die Schleife ist nich endloss weil sie endet wenn es 2 erreicht 
			System.out.println(i);
			   
			   
			   
		    }

		
	}	
}


